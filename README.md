# DPP.jl: Determinantal Point Processes in Julia

Determinantal Point Processes are point processes with repulsion properties: define an appropriate function for representing similarity between items, and a DPP will sample a subset of "representative" items, i.e. ones that does not contain two items that are too similar.

## State of the package

The package works, is presumably useful, but is under-documented. See docs/build/ directory for some basic features. 


## References

This package is used in the following pre-print: 

Tremblay, Nicolas, Simon Barthelmé, and Pierre-Olivier Amblard (2018) "Determinantal point processes for coresets." [arxiv.org/abs/1803.08700](https://arxiv.org/abs/1803.08700)

## Authors

Simon Barthelmé & Nicolas Tremblay, Gipsa-lab, CNRS. 

## See also 

For a full-featured DPP toolkit in Python, see
[DPPy](https://github.com/guilgautier/DPPy) by Guillaume Gautier.


