using DPP

indices = [1:20, 1:10]
dpp = DPP.FourierProjectionDPP(indices)

println("|X|=$(DPP.trace(dpp, :K))")
@time sample = generate_sample(dpp; rng=-1)

if dimension(dpp) == 2
    using Plots
    Plots.scatter(sample[1, :], sample[2, :], aspect_ratio=:equal)
end
