using DPP

indices = [1:2, 1:2, 1:2, 1:5, 1:5]
m_idx = DPP.multi_indices(indices)

N = size(m_idx, 2)
e_vals_K = rand(N)
dpp = DPP.FourierDPP(m_idx, e_vals_K, :K)
# e_vals_L = rand(N)
# dpp = DPP.FourierDPP(m_idx, e_vals_L, :L)

println("E[|X|]=$(DPP.trace(dpp, :K))")

@time sample = DPP.generate_sample(dpp; rng=-1)

if dimension(dpp) == 2
    using Plots
    Plots.scatter(sample[1, :], sample[2, :], aspect_ratio=:equal)
end
