# For dcftp to work in practice, the correlation kernel K of the DPP must have
# - relatively small eigenvalues
# - of fast decaying eigenvalues
# It is hopeless to sample, efficiently, large near projection DPPs with dominated CFTP.

using DPP

indices = [1:2, 1:2, 1:2, 1:5]
m_idx = DPP.multi_indices(indices)
N = size(m_idx, 2)
e_vals_K = 0.5 * rand(N)  # fill(1 - 1/N, N) may take a long time!
dpp = DPP.FourierDPP(m_idx, e_vals_K, :K)
# e_vals_L = 0.5 * rand(N)
# dpp = DPP.FourierDPP(m_idx, e_vals_L, :L)

println("E[|X|]=$(DPP.trace(dpp, :K))")
@time sample = DPP.generate_sample_dcftp(dpp; rng=-1)

if dimension(dpp) == 2
    using Plots
    Plots.scatter(sample[1, :], sample[2, :], aspect_ratio=:equal)
end
