using DPP

# μ(dx) = (1-x)^a (1+x)^b dx
a, b = [-0.5, -0.5], [-0.5, -0.5]
indices = [1:20, 1:10]

@assert length(a) == length(b) == length(indices)

dpp = DPP.JacobiProjectionDPP(a, b, indices)

println("|X|=$(DPP.trace(dpp, :K))")

@time sample = generate_sample(dpp; rng=-1)

if dimension(dpp) == 2
    using Plots
    Plots.scatter(sample[1, :], sample[2, :], aspect_ratio=:equal)
end
