using DPP

# μ(dx) = (1-x)^a (1+x)^b dx
a, b = [-0.5, -0.5], [-0.5, -0.5]
indices = [1:20, 1:20]

@assert length(a) == length(b) == length(indices)

m_idx = DPP.multi_indices(indices)
N = size(m_idx, 2)
e_vals_K = rand(N)
dpp = DPP.JacobiDPP(a, b, m_idx, e_vals_K, :K)
# e_vals_L = rand(N)
# dpp = DPP.JacobiDPP(a, b, m_idx, e_vals_L, :L)

println("E[|X|]=$(DPP.trace(dpp, :K))")
@time sample = DPP.generate_sample(dpp; rng=-1)

if dimension(dpp) == 2
    using Plots
    Plots.scatter(sample[1, :], sample[2, :], aspect_ratio=:equal)
end
