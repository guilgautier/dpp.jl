# T::Element type / U::Kernel value

abstract type AbstractDPP{T,U<:Union{Real,Complex}} <: AbstractPointProcess{T} end

const KERNEL_SYMBOLS = Set([:K, :L])
function check_kernel_symbol(sym::Symbol)
    sym ∉ KERNEL_SYMBOLS && error("sym=$(sym) ∉ KERNEL_SYMBOLS=$(KERNEL_SYMBOLS)")
end
function check_kernel_symbol(dpp::AbstractDPP, sym::Symbol)
    check_kernel_symbol(sym)
end

function check_bounds_eig_vals(kernel::Symbol, eig_vals)
    check_kernel_symbol(kernel)
    if kernel === :K
        @assert all(x->(0 <= x <= 1), eig_vals)
    else  # if kernel === :L
        @assert all(x->(x >= 0), eig_vals)
    end
end

function papangelou_conditional_intensity(
        dpp::AbstractDPP,
        x,
        X
)::Real
    return schur_complement(dpp, :L, x, X)
end
