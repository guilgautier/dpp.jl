function _schur_complement_generic(
        dpp::AbstractDPP,
        kernel_symbol::Symbol,  # :K, :L
        X,
        Y,
        force_cholesky::Bool=false
)
    check_kernel_symbol(dpp, kernel_symbol)
    length(Y) == 0 && return kernel(dpp, kernel_symbol, X)

    K_XX = kernel(dpp, kernel_symbol, X)
    K_YX = kernel(dpp, kernel_symbol, Y, X)
    K_YY = kernel(dpp, kernel_symbol, Y)
    if force_cholesky
        return K_XX - K_YX' * (LA.cholesky!(K_YY) \ K_YX)
    else
        return K_XX - K_YX' * (LA.lu!(K_YY) \ K_YX)
    end
end

function _schur_complement_scalar_2_points(
        dpp::AbstractDPP,
        sym::Symbol,  # :K, :L
        x,
        y
)::Real
    check_kernel_symbol(dpp, sym)
    return kernel(dpp, sym, x) - abs2(kernel(dpp, sym, x, y)) / kernel(dpp, sym, y)
end

function _schur_complement_scalar_ratio_det(
        dpp::AbstractDPP,
        kernel_symbol::Symbol,  # :K, :L
        x,
        X,
        force_cholesky::Bool=false
)::Real
    check_kernel_symbol(dpp, kernel_symbol)
    length(X) == 0 && return kernel(dpp, kernel_symbol, x)

    Y = copy(X)
    if Y isa AbstractMatrix
        Y = hcat(Y, x)
    else
        push!(Y, x)
    end

    log_det_X = log_det_Y = 0.0
    if force_cholesky
        log_det_X, _ = LA.logabsdet(LA.cholesky!(kernel(dpp, kernel_symbol, X)))
        log_det_Y, _ = LA.logabsdet(LA.cholesky!(kernel(dpp, kernel_symbol, Y)))
    else
        log_det_X, _ = LA.logabsdet(LA.lu!(kernel(dpp, kernel_symbol, X)))
        log_det_Y, _ = LA.logabsdet(LA.lu!(kernel(dpp, kernel_symbol, Y)))
    end

    return exp(log_det_Y - log_det_X)
end

function _schur_complement_scalar(
        dpp::AbstractDPP,
        kernel_symbol::Symbol,  # :K, :L
        x,
        X,
        ratio_det::Bool=false,
        force_cholesky::Bool=false
)::Real
    length(X) >= length(dpp) && return 0.0
    if ratio_det
        return real(_schur_complement_scalar_ratio_det(dpp, kernel_symbol, x, X, force_cholesky))
    else
        return real(_schur_complement_generic(dpp, kernel_symbol, x, X, force_cholesky))
    end
end

function _schur_complement_matrix(
        dpp::AbstractDPP{T,U},
        kernel_symbol::Symbol,  # :K, :L
        X,
        Y,
        force_cholesky::Bool=false
)::LA.HermOrSym(U, Matrix{U}) where {T,U}
    if U <: Real
        return LA.Symmetric(_schur_complement_generic(dpp, kernel_symbol, X, Y, force_cholesky), :U)
    else
        return LA.Hermitian(_schur_complement_generic(dpp, kernel_symbol, X, Y, force_cholesky), :U)
    end
end

function schur_complement(
        dpp::AbstractDPP{T,U},
        kernel_symbol::Symbol,  # :K, :L
        x::T,
        y::T;
        force_cholesky::Bool=false
) where {T,U}
    return _schur_complement_scalar_2_points(dpp, kernel_symbol, x, y)
end
function schur_complement(
        dpp::AbstractDPP{Vector{T},U},
        kernel_symbol::Symbol,  # :K, :L
        x::AbstractVector{T},
        y::AbstractVector{T};
        force_cholesky::Bool=false
) where {T,U}
    return _schur_complement_scalar_2_points(dpp, kernel_symbol, x, y)
end

function schur_complement(
        dpp::AbstractDPP{T,U},
        kernel_symbol::Symbol,  # :K, :L
        x::T,
        Y::Union{AbstractSet{T},AbstractVector{T}};
        det_ratio::Bool=false,
        force_cholesky::Bool=false
) where {T,U}
    return _schur_complement_scalar(dpp, kernel_symbol, x, Y, det_ratio, force_cholesky)
end
function schur_complement(
        dpp::AbstractDPP{Vector{T},U},
        kernel_symbol::Symbol,  # :K, :L
        x::AbstractVector{T},
        Y::Union{AbstractSet{Vector{T}},AbstractVector{Vector{T}},AbstractMatrix{T}};
        det_ratio::Bool=false,
        force_cholesky::Bool=false
) where {T,U}
    return _schur_complement_scalar(dpp, kernel_symbol, x, Y, det_ratio, force_cholesky)
end

function schur_complement(
        dpp::AbstractDPP{T,U},
        kernel_symbol::Symbol,  # :K, :L
        X::Union{AbstractSet{T},AbstractVector{T}},
        Y::Union{AbstractSet{T},AbstractVector{T}};
        force_cholesky::Bool=false
) where {T,U}
    return _schur_complement_matrix(dpp, kernel_symbol, X, Y, force_cholesky)
end
function schur_complement(
        dpp::AbstractDPP{Vector{T},U},
        kernel_symbol::Symbol,  # :K, :L
        X::Union{AbstractSet{Vector{T}},AbstractVector{Vector{T}},AbstractMatrix{T}},
        Y::Union{AbstractSet{Vector{T}},AbstractVector{Vector{T}},AbstractMatrix{T}};
        force_cholesky::Bool=false
) where {T,U}
    return _schur_complement_matrix(dpp, kernel_symbol, X, Y, force_cholesky)
end
