getRNG(seed::Integer = -1) = seed >= 0 ? Random.MersenneTwister(seed) : Random.GLOBAL_RNG
getRNG(seed::Union{Random.MersenneTwister,Random._GLOBAL_RNG}) = seed

function multi_indices(idx::Vector{T})::Matrix{Int64} where {T<:AbstractVector{Int64}}
    d, N = length(idx), prod(length.(idx))
    return reshape(collect(Iterators.flatten(Iterators.product(idx...))), d, N)
end

# Adapted from https://stackoverflow.com/questions/47564825/check-if-all-the-elements-of-a-julia-array-are-equal/47578613
@inline function allequal(x)::Bool
    length(x) < 2 && return true
    x1 = x[1]
    @inbounds for i in eachindex(x)
        x[i] == x1 || return false
    end
    return true
end
