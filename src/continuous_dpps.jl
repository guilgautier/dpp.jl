abstract type AbstractContinuousDPP{T,U} <: AbstractDPP{T,U} end

# function window end

# function dimension(dpp::AbstractContinuousProjectionDPP)::Int64 end
# function Base.length(dpp::AbstractContinuousProjectionDPP)::Int64 end  # Number of eigenfunctions

function Base.size(dpp::AbstractContinuousDPP)::NTuple{2,Int64}
    (dimension(dpp), length(dpp))
end


## Used in src/dominated_cftp.jl

isrepulsive(dpp::AbstractContinuousDPP) = true
isattractive(dpp::AbstractContinuousDPP) = false

## Kernel computations

# function eigen_value(dpp, n, kernel::Symbol) end
# function eigen_function(dpp{T,U}, n::Integer, x)::U end

function feature_vector!(
        phi_x::AbstractVector{U},
        dpp::AbstractContinuousDPP{T,U},
        kernel::Symbol,  # :K, :L
        x::T
) where {T,U}
    for n in eachindex(phi_x)
        phi_x[n] = sqrt(eigen_value(dpp, n, kernel)) * eigen_function(dpp, n, x)
    end
end
function feature_vector!(
        phi_x::AbstractVector{U},
        dpp::AbstractContinuousDPP{Vector{T},U},
        kernel::Symbol,  # :K, :L
        x::AbstractVector{T}
) where {T,U}
    for n in eachindex(phi_x)
        phi_x[n] = sqrt(eigen_value(dpp, n, kernel)) * eigen_function(dpp, n, x)
    end
end

function feature(
        dpp::AbstractContinuousDPP{Vector{T},U},
        kernel::Symbol,  # :K, :L
        x::AbstractVector{T}
)::Vector{U} where {T,U}
    phi_x = Vector{U}(undef, length(dpp))
    feature_vector!(phi_x, dpp, kernel, x)
    return phi_x
end
function feature(
        dpp::AbstractContinuousDPP{T,U},
        kernel::Symbol,  # :K, :L
        X::Union{AbstractSet{T},AbstractVector{T}}
)::Matrix{U} where {T,U}
    Phi_X = Matrix{U}(undef, length(dpp), length(X))
    for (phi_x, x) in zip(eachcol(Phi_X), X)
        feature_vector!(phi_x, dpp, kernel, x)
    end
    return Phi_X
end
function feature(
        dpp::AbstractContinuousDPP{Vector{T},U},
        kernel::Symbol,  # :K, :L
        X::AbstractMatrix{T}
)::Matrix{U} where {T,U}
    Phi_X = Matrix{U}(undef, length(dpp), size(X, 2))
    for (phi_x, x) in zip(eachcol(Phi_X), eachcol(X))
        feature_vector!(phi_x, dpp, kernel, x)
    end
    return Phi_X
#     mapslices(y->feature(dpp, kernel, y), x; dims=1)
end

function _kernel_value(
        dpp::AbstractContinuousDPP,
        kernel::Symbol,
        x
)::Real
    return LA.norm_sqr(feature(dpp, kernel, x))
end
function _kernel_value(
        dpp::AbstractContinuousDPP{T,U},
        kernel::Symbol,
        x,
        y
)::U where {T,U}
    # return feature(dpp, kernel, x)' * feature(dpp, kernel, y)
    return LA.dot(feature(dpp, kernel, x), feature(dpp, kernel, y))
end

function _kernel_vector(
        dpp::AbstractContinuousDPP{T,U},
        kernel::Symbol,
        X,
        y
)::Vector{U} where {T,U}
    # return feature(dpp, kernel, X)' * feature(dpp, kernel, y)
    K_Xy = Vector{U}(undef, X isa AbstractMatrix ? size(X, 2) : length(X))
    LA.BLAS.gemv!(U<:Real ? 'T' : 'C',
                  one(U),
                  feature(dpp, kernel, X),
                  feature(dpp, kernel, y),
                  zero(U),
                  K_Xy)
    return K_Xy
end

function _kernel_matrix(
        dpp::AbstractContinuousDPP{T,U},
        kernel::Symbol,
        X
)::LA.HermOrSym{U,Matrix{U}} where {T,U}
    # return feature(dpp, kernel, X)' * feature(dpp, kernel, X)
    if U<:Real
        return LA.Symmetric(LA.BLAS.syrk('U', 'T', 1.0, feature(dpp, kernel, X)), :U)
    else  # U<:Complex
        return LA.Hermitian(LA.BLAS.herk('U', 'C', 1.0, feature(dpp, kernel, X)), :U)
    end
end
function _kernel_matrix(
        dpp::AbstractContinuousDPP{T,U},
        kernel::Symbol,
        X,
        Y
)::Matrix{U} where {T,U}
    # return feature(dpp, kernel, X)' * feature(dpp, kernel, Y)
    return LA.BLAS.gemm(U<:Real ? 'T' : 'C',
                        'N',
                        feature(dpp, kernel, X),
                        feature(dpp, kernel, Y))
end

## Children

include("continuous/window.jl")
include("continuous/dominated_cftp.jl")

include("continuous/jacobi_polynomials.jl")

include("continuous/continuous_projection_dpps.jl")
include("continuous/sampling.jl")

const continuous_dpps = [
    "fourier",
    "jacobi"
]
for name in continuous_dpps
    include(joinpath("continuous", "$(name).jl"))
end
