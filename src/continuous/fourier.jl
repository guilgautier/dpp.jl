struct FourierDPP{T<:Vector{Float64},U<:Complex{Float64}} <: AbstractContinuousDPP{T,U}
    multi_idx::Matrix{Int64}
    eig_vals_K::Vector{Float64}
    eig_vals_L::Vector{Float64}
end

struct FourierProjectionDPP{T,U<:Complex{Float64}} <: AbstractContinuousProjectionDPP{T,U}
    multi_idx::Matrix{Int64}
end

## Constructors

# FourierDPP

function FourierDPP(
        multi_idx::Matrix{Int64},
        eig_vals::Vector{Float64},
        kernel::Symbol
)

    check_kernel_symbol(kernel)
    check_bounds_eig_vals(kernel, eig_vals)

    @assert length(eig_vals) == size(multi_idx, 2)

    if kernel === :K
        eig_vals_K = eig_vals
        eig_vals_L = eig_vals_K ./ (1.0 .- eig_vals_K)
        return FourierDPP{Vector{Float64},Complex{Float64}}(multi_idx, eig_vals_K, eig_vals_L)
    else  # if kernel === :L
        eig_vals_L = eig_vals
        eig_vals_K = eig_vals_L ./ (1.0 .+ eig_vals_L)
        return FourierDPP{Vector{Float64},Complex{Float64}}(multi_idx, eig_vals_K, eig_vals_L)
    end

end

function FourierDPP(
        idx::Vector{T},
        eig_vals::Vector{Float64},
        kernel::Symbol
) where {T<:AbstractVector{Int64}}
    FourierDPP(multi_indices(idx), eig_vals, kernel)
end

# FourierProjectionDPP

FourierProjectionDPP(multi_idx::Matrix{Int64}) = FourierProjectionDPP{Vector{Float64},Complex{Float64}}(multi_idx)
FourierProjectionDPP(idx::Vector{T}) where {T<:AbstractVector{Int64}} = FourierProjectionDPP(multi_indices(idx))

## Methods

dimension(dpp::Union{FourierDPP,FourierProjectionDPP}) = size(dpp.multi_idx, 1)
Base.length(dpp::Union{FourierDPP,FourierProjectionDPP}) = size(dpp.multi_idx, 2)

function window(dpp::Union{FourierDPP,FourierProjectionDPP})
    SquareWindow(fill(0.0, dimension(dpp)), 1.0)
end

support(dpp::Union{FourierDPP,FourierProjectionDPP}) = window(dpp)

function weight(
        dpp::Union{FourierDPP{Vector{T},U},FourierProjectionDPP{Vector{T},U}},
        x::AbstractVector{T}
)::Float64 where {T,U}
    return one(Float64)
end

function trace(dpp::FourierDPP, kernel::Symbol=:K)
    check_kernel_symbol(dpp, kernel)
    return sum(kernel === :K ? dpp.eig_vals_K : dpp.eig_vals_L)
end
function trace(dpp::FourierProjectionDPP, kernel::Symbol=:K)
    check_kernel_symbol(dpp, kernel)
    return length(dpp)  # sum(1.0)
end

## Kernel computation

function eigen_value(
        dpp::FourierDPP,
        n::Integer,
        sym::Symbol  # :K, :L
)::Real
    check_kernel_symbol(dpp, sym)
    return kernel === :K ? dpp.eig_vals_K[n] : dpp.eig_vals_L[n]
end

# exp(2iπ <j,x>)
function eigen_function(
        dpp::Union{FourierDPP{Vector{T},U},FourierProjectionDPP{Vector{T},U}},
        j_n::AbstractVector{Int64},
        x::AbstractVector{T}
)::U where {T<:Float64,U<:Complex{Float64}}
    return cis(2π * LA.dot(j_n, x))
end
function eigen_function(
        dpp::Union{FourierDPP{Vector{T},U},FourierProjectionDPP{Vector{T},U}},
        n::Integer,
        x::AbstractVector{T}
)::U where {T<:Float64,U<:Complex{Float64}}
    return eigen_function(dpp, dpp.multi_idx[:, n], x)
end

# K(x,x) = K₀(x-x) = K₀(0) = N exp(2π 0) = N
function _kernel_value(
        dpp::FourierProjectionDPP{T,U},
        x
)::Float64 where {T,U}
    convert(Float64, length(dpp))
end

# ∑ⱼ exp(2iπ <j,y-x>)
function _kernel_value(
        dpp::FourierProjectionDPP{T,U},
        sym::Symbol,
        x,
        y
)::U where {T,U}
    # Use K(x,y) = K₀(y-x) instead of default <Φ(x),Φ(y)>
    K_xy = zero(U)
    z = y - x
    for (n, j_n) in enumerate(eachcol(dpp.multi_idx))
        K_xy += eigen_value(dpp, n, sym) * eigen_function(dpp, j_n, z)
    end
    return K_xy
end

## Sampling

### FourierDPP

# Dominated CFTP

# see function generate_sample_dcftp end in src/dominated_cftp.jl
# see function papangelou_conditional_intensity end in src/abstract_dpp.jl
function upper_bound_papangelou_conditional_intensity(dpp::FourierDPP)
    return L(dpp, rand(window(dpp)))  # trace(dpp, :L)
end

# Spectral
function generate_sample(
        dpp::FourierDPP{Vector{T}, U};
        nb_trials_max=10_000,
        rng=-1
)::Matrix{Float64} where {T, U}
    rng = getRNG(rng)

    idx_selected = select_eigen_function_indices(dpp; rng=-1)
    proj_dpp = FourierProjectionDPP(dpp.multi_idx[:, idx_selected])

    return generate_sample(proj_dpp; nb_trials_max=nb_trials_max, rng=rng)
end

### FourierProjectionDPP

# Sample from 1/N K(x,x) μ(dx) = μ(dx) = 1_{[0,1]^d} dx
function sample_marginal!(
        x::AbstractVector{T},
        dpp::FourierProjectionDPP{Vector{T},U};
        rng=-1
) where {T<:Float64,U<:Complex{Float64}}
    rng = getRNG(rng)
    return rand!(rng, Distributions.Uniform(0.0, 1.0), x)
end
