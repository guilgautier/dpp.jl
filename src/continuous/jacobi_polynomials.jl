weight_jacobi(x, a, b) = (one(x) - x)^a * (one(x) + x)^b

function jacobi(a::Real, b::Real)::OPQ.AbstractJacobi{Float64}
    if a == b == 0
        return OPQ.Legendre()
    elseif a == b == -0.5
        return OPQ.Chebyshev()
    end
    return OPQ.jacobi(a, b)
end

orthonormal_jacobi(a, b) = OPQ.Normalized(jacobi(a, b))

# Be careful, P_0 corresponds to P_1 in OrthogonalPolynomialsQuasi (OPQ)
function jacobi_square_norm(a::Real, b::Real, n::Integer; log_::Bool=true)::Real
    n == 0 && return 2^(a + b + one(a)) * SF.beta(a + one(a), b + one(b))

    log_ && return exp(log(2) * (a + b + one(a))
                        - log(2 * n + 1 + (a + b))
                        + SF.loggamma(n + 1 + a)
                        + SF.loggamma(n + 1 + b)
                        - SF.loggamma(n + 1 + (a + b))
                        - SF.loggamma(n + 1))

    return (2^(a + b + one(1))
            / (2 * n + 1 + (a + b))
            * SF.gamma(n + 1 + a)
            * SF.gamma(n + 1 + b)
            / SF.gamma(n + 1 + (a + b))
            / SF.gamma(n + 1))
end
