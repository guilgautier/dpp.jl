abstract type AbstractWindow end

# Πᵢ [cᵢ, cᵢ + wᵢ]
struct RectangleWindow{T<:Float64} <: AbstractWindow
    # Corner
    c::Vector{T}
    # Width
    w::Vector{T}
end

function RectangleWindow(c::AbstractVector, w::AbstractVector)
    @assert length(c) == length(w)
    return RectangleWindow{Float64}(convert(Vector{Float64}, c),
                                    convert(Vector{Float64}, w))
end

# Πᵢ [cᵢ, cᵢ + w]
struct SquareWindow{T<:Float64} <: AbstractWindow
    # Corner
    c::Vector{T}
    # Width
    w::T
end

function SquareWindow(c::AbstractVector, w::Real)
    return SquareWindow{Float64}(convert(Vector{Float64}, c),
                                 convert(Float64, w))
end

dimension(win::Union{RectangleWindow, SquareWindow})::Int = length(win.c)

perimeter(win::RectangleWindow) = sum(win.w)
perimeter(win::SquareWindow) = win.w * dimension(win)

volume(win::RectangleWindow) = prod(win.w)
volume(win::SquareWindow) = win.w^dimension(win)

function Base.rand(
    win::Union{SquareWindow{T}, RectangleWindow{T}};
    rng=-1
)::Union{T, Vector{T}} where {T<:Float64}
    rng = getRNG(rng)
    d = dimension(win)
    if d == 1
        return win.c + win.w * rand(rng, T)
    else
        return win.c .+ win.w .* rand(rng, T, d)
    end
end
