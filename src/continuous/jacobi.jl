# struct JacobiDPP{T<:Vector{Float64},U<:Float64,V<:AbstractVector{Int64}} <: AbstractContinuousDPP{T,U}

# Naive implementations (not optimized) of JacobiDPP and JacobiProjectionDPP

struct JacobiDPP{T<:Vector{Float64},U<:Float64} <: AbstractContinuousDPP{T,U}
    a::Vector{Float64}
    b::Vector{Float64}
    OPs::Vector{OPQ.Normalized}
    multi_idx::Matrix{Int64}
    eig_vals_K::Vector{Float64}
    eig_vals_L::Vector{Float64}
end

struct JacobiProjectionDPP{T<:Vector{Float64},U<:Float64} <: AbstractContinuousProjectionDPP{T,U}
    a::Vector{Float64}
    b::Vector{Float64}
    OPs::Vector{OPQ.Normalized}
    multi_idx::Matrix{Int64}
end

## Constructors

# Jacobi

function JacobiDPP(
        a::Vector{T},
        b::Vector{T},
        multi_idx::Matrix{Int64},
        eig_vals::Vector{T},
        kernel::Symbol=:K
) where {T<:Float64}
    check_kernel_symbol(kernel)
    check_bounds_eig_vals(kernel, eig_vals)

    d, N = size(multi_idx)
    @assert length(a) == length(b) == d

    OPs = orthonormal_jacobi.(a, b)

    @assert length(eig_vals) == N

    if kernel === :K
        eig_vals_K = eig_vals
        eig_vals_L = eig_vals_K ./ (1.0 .- eig_vals_K)
        return JacobiDPP{Vector{Float64},Float64}(a, b, OPs, multi_idx, eig_vals_K, eig_vals_L)
    else  # if kernel === :L
        eig_vals_L = eig_vals
        eig_vals_K = eig_vals_L ./ (1.0 .+ eig_vals_L)
        return JacobiDPP{Vector{Float64},Float64}(a, b, OPs, multi_idx, eig_vals_K, eig_vals_L)
    end
end

function JacobiDPP(
        a::Vector{T},
        b::Vector{T},
        idx::Vector{U},
        eig_vals::Union{T,Vector{T}},
        kernel::Symbol=:K
) where {T<:Float64, U<:AbstractVector{Int64}}
    return JacobiDPP(a, b, multi_indices(idx), e_vals, kernel)
end

# JacobiProjection

function JacobiProjectionDPP(
        a::Vector{T},
        b::Vector{T},
        multi_idx::Matrix{V}
) where {T<:Float64, V<:Int64}
    d = size(multi_idx, 1)
    @assert length(a) == length(b) == d
    OPs = orthonormal_jacobi.(a, b)
    return JacobiProjectionDPP{Vector{Float64},Float64}(a, b, OPs, multi_idx)
end

function JacobiProjectionDPP(
        a::Vector{T},
        b::Vector{T},
        idx::Vector{U}
) where {T<:Real, U<:AbstractVector{Int64}}
    return JacobiProjectionDPP(a, b, multi_indices(idx))
end

## Methods

dimension(dpp::Union{JacobiDPP,JacobiProjectionDPP}) = size(dpp.multi_idx, 1)
Base.length(dpp::Union{JacobiDPP,JacobiProjectionDPP}) = size(dpp.multi_idx, 2)

function window(dpp::Union{JacobiDPP,JacobiProjectionDPP})
    return SquareWindow(fill(-1.0, dimension(dpp)), 2.0)
end

support(dpp::Union{JacobiDPP,JacobiProjectionDPP}) = window(dpp)

function weight(
        dpp::Union{JacobiDPP{Vector{T},U},JacobiProjectionDPP{Vector{T},U}},
        x::AbstractVector{T}
)::Float64 where {T,U}
    return prod(weight_jacobi.(x, dpp.a, dpp.b))
end

function trace(dpp::JacobiDPP, kernel::Symbol=:K)
    check_kernel_symbol(dpp, kernel)
    return sum(kernel === :K ? dpp.eig_vals_K : dpp.eig_vals_L)
end
function trace(dpp::JacobiProjectionDPP, kernel::Symbol=:K)
    check_kernel_symbol(dpp, kernel)
    return length(dpp)  # sum(1.0)
end

## Kernel computation

function eigen_value(
        dpp::JacobiDPP,
        n::Integer,
        sym::Symbol  # :K, :L
)::Real
    check_kernel_symbol(dpp, sym)
    return kernel === :K ? dpp.eig_vals_K[n] : dpp.eig_vals_L[n]
end

# Πᵢ Pⁱnᵢ(xᵢ)
function eigen_function(
        dpp::Union{JacobiDPP{Vector{T},U},JacobiProjectionDPP{Vector{T},U}},
        n_multi::AbstractVector{Int64},
        x::AbstractVector{T}
)::U where {T<:Float64,U<:Float64}
    return prod(P[xᵢ, nᵢ] for (P, xᵢ, nᵢ) in zip(dpp.OPs, x, n_multi))
    # return prod(getindex.(dpp.OPs, x, n_multi))
end
function eigen_function(
        dpp::Union{JacobiDPP{Vector{T},U},JacobiProjectionDPP{Vector{T},U}},
        n::Integer,
        x::AbstractVector{T}
)::U where {T<:Float64,U<:Float64}
    return eigen_function(dpp, dpp.multi_idx[:, n], x)
end

## Sampling

# Spectral
function generate_sample(
        dpp::JacobiDPP{Vector{T}, U};
        nb_trials_max=10_000,
        rng=-1
)::Matrix{Float64} where {T, U}
    rng = getRNG(rng)

    idx_selected = select_eigen_function_indices(dpp; rng=-1)
    proj_dpp = JacobiProjectionDPP(dpp.a, dpp.b, dpp.multi_idx[:, idx_selected])

    return generate_sample(proj_dpp; nb_trials_max=nb_trials_max, rng=rng)
end

### JacobiProjectionDPP
function sample_marginal_proposal!(
        x::AbstractVector{T},
        dpp::JacobiProjectionDPP{Vector{T},U};
        rng=-1
) where {T,U}
    rng = getRNG(rng)
    rand!(rng, Distributions.Arcsine(-1.0, 1.0), x)  # 1/(π(1-x^2))
end

function sample_marginal!(
        x::AbstractVector{T},
        dpp::JacobiProjectionDPP{Vector{T},U};
        rng=-1,
        nb_trials_max=100_000
) where {T,U}
    rng = getRNG(rng)
    d, N = size(dpp)

    i = rand(rng, 1:N)
    multi_i = dpp.multi_idx[:, i]
    Mᵢ = rejection_bound(dpp, multi_i)

    pi_d = π^d
    a_05, b_05 = dpp.a .+ 0.5, dpp.b .+ 0.5

    for _ in 1:nb_trials_max
        sample_marginal_proposal!(x, dpp; rng=rng)  # proposal Arcsine(-1.0, 1.0)
        ϕᵢ_x_2 = eigen_function(dpp, multi_i, x)^2
        ratio_w_weq = pi_d * prod(weight_jacobi.(x, a_05, b_05))
        ((rand(rng) * Mᵢ) < (ϕᵢ_x_2 * ratio_w_weq)) && break
    end
end

rejection_bound(jac::OPQ.Chebyshev, n::Integer) = rejection_bound(-0.5, -0.5, n)
rejection_bound(jac::OPQ.Jacobi, n::Integer) = rejection_bound(jac.a, jac.b, n)
function rejection_bound(dpp::JacobiProjectionDPP, n::AbstractVector{Int64})::Float64
    return prod(rejection_bound.(dpp.a, dpp.b, n))
end

function rejection_bound(a::Real, b::Real, n::Integer)::Float64
    @assert n >= 1
    @assert abs(a) <= 0.5 && abs(b) <= 0.5

    a == b == -0.5 && return n == 1 ? 1.0 : 2.0

    if n == 1  # P0
#         norm_sqr_P0 = jacobi_square_norm(a, b, n)
        norm_sqr_P0 = 2^(a + b + one(a)) * SF.beta(a + one(a), b + one(b))
        a == b && return π / norm_sqr_P0

        md = (b - a) / (a + b + one(a))  # mode of (1-x)^(1/2 + a) (1+x)^(1/2 + b)
        return (π / norm_sqr_P0) * (1 - md)^(0.5 + a) * (1 + md)^(0.5 + b)
    else
        m, M = minmax(a, b)
        return exp(log(2)
                    + SF.loggamma(n + 1 + a + b)
                    + SF.loggamma(n + 1 + M)
                    - SF.loggamma(n + 1)
                    - 2 * M * log(n + 0.5 * (a + b + 1))
                    - SF.loggamma(n + 1 + m))
    end
end

function sample_univariate_jacobi_beta_ensemble(
        N::Integer,
        j_a::Real,
        j_b::Real,
        β=2;
        rng=-1
)::Matrix{Float64}
    rng = getRNG(rng)

    β_2 = β / 2

    c = zeros(Float64, 2*N-1)
    for n in 1:N
        _a = β_2 * (N - n + j_a)
        _b = β_2 * (N - n + j_b)
        c[2n-1] = rand(Distributions.Beta(_a, _b))

        if n < N
            _a = β_2 * (N - n)
            _b = β_2 * (N - n - 1 + j_a + j_b)
            c[2n] = rand(Distributions.Beta(_a, _b))
        end
    end

    a = zeros(Float64, N)
    b = zeros(Float64, N-1)

    a[1] = c[1]
    b[1] = c[1] * (1 - c[2]) * c[2]

    for n in 2:N
        a[n] = (1 - c[2n-3]) * c[2n-2] + (1 - c[2n-2]) * c[2n-1]
        if n < N
            b[n] = sqrt((1 - c[2n-2]) * c[2n-1] * (1 - c[2n-1]) * c[2n])
        end
    end

    return reshape(LA.eigvals!(LA.SymTridiagonal(a, b)), 1, N)
end
