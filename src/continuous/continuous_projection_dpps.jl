abstract type AbstractContinuousProjectionDPP{T,U} <: AbstractContinuousDPP{T,U} end

function trace(dpp::AbstractContinuousProjectionDPP, kernel::Symbol=:K)
    check_kernel_symbol(dpp, kernel)
    return length(dpp)  # sum(1.0)
end

## Kernel computations

error_projection_dpp_has_no_likelihood_kernel() = error("Projection DPPs have no likelihood (:L) kernel")

function check_kernel_symbol(dpp::AbstractContinuousProjectionDPP, kernel::Symbol)
    kernel != :K && error_projection_dpp_has_no_likelihood_kernel()
end

function eigen_value(
        dpp::AbstractContinuousProjectionDPP,
        n::Integer,
        kernel::Symbol
)::Float64
    check_kernel_symbol(dpp, kernel)
    return one(Float64)
end

function feature_vector!(
        phi_x::AbstractVector{U},
        dpp::AbstractContinuousProjectionDPP{T,U},
        x::T
) where {T, U}
    for n in eachindex(phi_x)
        phi_x[n] = eigen_function(dpp, n, x)
    end
end
function feature_vector!(
        phi_x::AbstractVector{U},
        dpp::AbstractContinuousProjectionDPP{Vector{T},U},
        x::AbstractVector{T}
) where {T, U}
    for n in eachindex(phi_x)
        phi_x[n] = eigen_function(dpp, n, x)
    end
end
