# Sampling

## Projection DPPs

function sample_marginal!(
        x::AbstractVector{T},
        dpp::AbstractContinuousProjectionDPP{Vector{T}, U};
        rng=-1
) where {T, U}
    error("Implement you own method")
end

function orthogonalize!(
        psi_x::AbstractVector{T},
        Psi::AbstractMatrix{T}
) where {T}
    psi_x .-= Psi * (Psi' * psi_x)
    # LA.BLAS.gemv!('N', -1.0, Psi, Psi' * psi_x,  1.0, psi_x)
end

function generate_sample(
        dpp::AbstractContinuousProjectionDPP{Vector{T}, U};
        nb_trials_max=10_000,
        rng=-1
)::Matrix{T} where {T, U}
    rng = getRNG(rng)
    d, N = size(dpp)

    points = Matrix{T}(undef, d, N)
    x = Vector{T}(undef, d)

    Psi = Matrix{U}(undef, N, N)
    psi_x = Vector{U}(undef, N)

    for n in 1:N
        trial = 1
        Psi_ = @view Psi[:, 1:n-1]
        while trial <= nb_trials_max
            sample_marginal!(x, dpp; rng=rng)  # ~1/N K(x,x) μ(dx)
            K_xx = K(dpp, x)
            feature_vector!(psi_x, dpp, x)
            orthogonalize!(psi_x, Psi_)
            schur_complement = LA.norm_sqr(psi_x)
            if rand(rng) < (schur_complement / K_xx)
                points[:, n] .= x
                Psi[:, n] = psi_x / sqrt(schur_complement)  # normalize
                break
            end
            trial += 1
        end
    end

    return points
end

## Generic DPPs

# Spectral Method see generate_sample above for sampling from ProjectionDPP(K)
function select_eigen_function_indices(
        dpp::AbstractContinuousDPP{Vector{T}, U};
        rng=-1
)::Vector{Int64} where {T, U}
    rng = getRNG(rng)
    return [i for (i, λᵢ) in enumerate(dpp.eig_vals_K) if rand(rng) < λᵢ]
end

# Dominated CFTP, see src/dominated_cftp.jl for sampling from DPP(L)
