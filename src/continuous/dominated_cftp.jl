# Require
# function papangelou_conditional_intensity end
# function upper_bound_papangelou_conditional_intensity end
# function window end

function generate_sample_dcftp(
        pp::AbstractPointProcess{T};
        n₀::Int64=1,
        rng=-1
)::Set{T} where {T}

    @assert n₀ >= 1
    rng = getRNG(rng)

    win = window(pp)
    β = upper_bound_papangelou_conditional_intensity(pp)
    birth_rate = β * volume(win)

    k = rand(rng, Distributions.Poisson(birth_rate))
    D = Set{T}(rand(win; rng=rng) for _ in 1:k)     # Dominating process

    M = Vector{Float64}()  # Marking process
    R = Vector{T}()  # Recording process

    steps = -1:-1:-n₀
    while true
        backward_update!(D, M, R, steps, birth_rate, win; rng=rng)
        coupling, L = forward_coupling(D, M, R, pp, β)
        coupling && return L
        steps = (steps.stop-1):-1:(2*steps.stop)
    end
end

function backward_update!(
        D::Set{T},          # Dominating process
        M::Vector{Float64}, # Marking process
        R::Vector{T},       # Recording process
        steps::StepRange,         # Number of backward steps
        birth_rate::Real,
        window::AbstractWindow;
        rng=-1
) where {T}

    rng = getRNG(rng)
    for _ in steps
        card_D = length(D)
        if rand(rng) < card_D / (birth_rate + card_D)
            # forward death (delete) ≡ backward birth (pushfirst)
            x = rand(rng, D)
            delete!(D, x)
            pushfirst!(R, x)
            pushfirst!(M, rand(rng))
        else
            # forward birth (push) ≡ backward death (pushfirst)
            x = rand(window; rng)
            push!(D, x)
            pushfirst!(R, x)
            pushfirst!(M, 0.0)
        end
    end
end

function forward_coupling(
        D::Set{T},           # Dominating process
        M::Vector{Float64},  # Marking process
        R::Vector{T},         # Recording process
        pp::AbstractPointProcess{T},
        β::Real             # Upper bound on papangelou conditional intensity
) where {T}
    # L ⊆ X ⊆ U ⊆ D, where X is the target process
    L, U = empty(D), copy(D)
    for (m, x) in zip(M, R)
        if m > 0  # if birth occured in D
            if isrepulsive(pp)
                if m < papangelou_conditional_intensity(pp, x, U) / β
                    push!(L, x)
                    push!(U, x)
                elseif m < papangelou_conditional_intensity(pp, x, L) / β
                    push!(U, x)
                end
            elseif isattractive(pp)
                if m < papangelou_conditional_intensity(pp, x, L) / β
                    push!(L, x)
                    push!(U, x)
                elseif m < papangelou_conditional_intensity(pp, x, U) / β
                    push!(U, x)
                end
            end
        else  # if death occured in D
            delete!(L, x)
            delete!(U, x)
        end
    end

    # Check coalescence L = U (Since L ⊆ U: L = U ⟺ |L| = |U|)
    return length(L) == length(U), L
end
