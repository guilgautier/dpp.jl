module DPP
using LinearAlgebra,StatsBase,Combinatorics,Distances,MLKernels
import Base.show,Base.getindex,LinearAlgebra.diag
export nitems,
        maxrank,
        sample,
        marginal_kernel,
        gaussker,
        rescale!,
        AbstractLEnsemble,
        FullRankEnsemble,
        LowRankEnsemble,
        ProjectionEnsemble,
        PPEnsemble,
        log_prob,
        polyfeatures,
        rff,
        inclusion_prob,
        cardinal,
        greedy_subset,
        esp,
        kl_divergence,
        total_variation

include("lensemble.jl")
include("kdpp.jl")
include("saddlepoint.jl")
include("features.jl")
include("sampling.jl")
include("subset_kernels.jl")
include("greedy.jl")
include("ppensembles.jl")
include("kldiv.jl")


# Continuous DPPs

# T::Element type
abstract type AbstractPointProcess{T} end

const LA = LinearAlgebra

using Random, Distributions

import OrthogonalPolynomialsQuasi
const OPQ = OrthogonalPolynomialsQuasi

import SpecialFunctions
const SF = SpecialFunctions

using Distances

export

    length,
    size,

    # generic types
    AbstractPointProcess,
    AbstractDPP,
    AbstractContinuousDPP,
    AbstractContinuousProjectionDPP,

    # Types of DPPs
    FourierDPP,
    FourierProjectionDPP,
    JacobiDPP,
    JacobiProjectionDPP,

    # methods
    dimension,
    kernel, K, L,
    schur_complement,

    # sampling
    generate_sample_dcftp,
    generate_sample,

    # Misc
    AbstractWindow,
    RectangleWindow,
    SquareWindow

##### specific continuous projection DPPs #####
include("utils.jl")

include("abstract_dpp.jl")
include("kernel.jl")
include("schur_complement.jl")

include("continuous_dpps.jl")

end # module
