function _kernel_value(
        dpp::AbstractDPP{T,U},
        sym::Symbol,  # :K, :L
        x,
        y
)::U where {T,U}
    check_kernel_symbol(dpp, sym)
    error("Implement your own kernel")
end
function _kernel_value(
        dpp::AbstractDPP,
        sym::Symbol,  # :K, :L
        x
)::Real
    check_kernel_symbol(dpp, sym)
    return real(_kernel_value(dpp, sym, x, x))
end

function _kernel_vector(
        dpp::AbstractDPP{T,U},
        sym::Symbol,  # :K, :L
        X,
        y
)::Vector{U} where {T,U}
    check_kernel_symbol(dpp, sym)
    return [_kernel_value(dpp, sym, x, y) for x in X]
end

function _kernel_matrix(
        dpp::AbstractDPP{T,U},
        sym::Symbol,  # :K, :L
        X,
        Y
)::Matrix{U} where {T,U}
    check_kernel_symbol(dpp, sym)
    return [_kernel_value(dpp, sym, x, y) for x in X, y in Y]
end
function _kernel_matrix(
        dpp::AbstractDPP{T,U},
        sym::Symbol,  # :K, :L
        X
)::LA.HermOrSym{U,Matrix{U}} where {T,U}
    check_kernel_symbol(dpp, sym)

    N = length(X)
    K_XX = zeros(U, N, N)
    for j in 1:N
        for i in 1:j
            if i == j
                K_XX[i, i] = _kernel_value(dpp, sym, X[i])
            else
                K_XX[i, j] = _kernel_value(dpp, sym, X[i], X[j])
            end
        end
    end
    if U<:Real
        # return LA.Symmetric(_kernel_matrix(dpp, sym, X, X))
        return LA.Symmetric(K_XX, :U)
    else  # U<:Complex
        # return LA.Symmetric(_kernel_matrix(dpp, sym, X, X))
        return LA.Hermitian(K_XX, :U)
    end
end

K(dpp::AbstractDPP, x) = kernel(dpp, :K, x)
K(dpp::AbstractDPP, x, y) = kernel(dpp, :K, x, y)

L(dpp::AbstractDPP, x) = kernel(dpp, :L, x)
L(dpp::AbstractDPP, x, y) = kernel(dpp, :L, x, y)


## Kernel value K_xx, K_xy

function kernel(
        dpp::AbstractDPP{T,U},
        sym::Symbol,
        x::T
)::Real where {T,U}
    return _kernel_value(dpp, sym, x)
end
function kernel(
        dpp::AbstractDPP{Vector{T},U},
        sym::Symbol,
        x::AbstractVector{T}
)::Real where {T,U}
    return _kernel_value(dpp, sym, x)
end

function kernel(
        dpp::AbstractDPP{T,U},
        sym::Symbol,
        x::T,
        y::T
)::U where {T,U}
    return _kernel_value(dpp, sym, x, y)
end
function kernel(
        dpp::AbstractDPP{Vector{T},U},
        sym::Symbol,
        x::AbstractVector{T},
        y::AbstractVector{T}
)::U where {T,U}
    return _kernel_value(dpp, sym, x, y)
end

## Kernel vector K_Xy, K_yX = K_Xy'

function kernel(
        dpp::AbstractDPP{T,U},
        sym::Symbol,
        X::Union{AbstractSet{T}, AbstractVector{T}},
        y::T
)::Vector{U} where {T,U}
    return _kernel_vector(dpp, sym, X, y)
end
function kernel(
        dpp::AbstractDPP{Vector{T},U},
        sym::Symbol,
        X::Union{AbstractSet{Vector{T}}, AbstractVector{Vector{T}}, AbstractMatrix{T}},
        y::AbstractVector{T}
)::Vector{U} where {T,U}
    return _kernel_vector(dpp, sym, X, y)
end

function kernel(
        dpp::AbstractDPP{T,U},
        sym::Symbol,
        y::T,
        X::Union{AbstractSet{T}, AbstractVector{T}}
)::LA.AdjointVector{U,Vector{U}} where {T,U}
    return _kernel_vector(dpp, sym, X, y)'
end
function kernel(
        dpp::AbstractDPP{Vector{T},U},
        sym::Symbol,
        y::AbstractVector{T},
        X::Union{AbstractSet{Vector{T}}, AbstractVector{Vector{T}}, AbstractMatrix{T}}
)::LA.AdjointVector{U,Vector{U}} where {T,U}
    return _kernel_vector(dpp, sym, X, y)'
end

## Kernel Matrix K_XX, K_XY

function kernel(
        dpp::AbstractDPP{T,U},
        sym::Symbol,
        X::Union{AbstractSet{T}, AbstractVector{T}}
)::LA.HermOrSym{U,Matrix{U}} where {T,U}
    return _kernel_matrix(dpp, sym, X)
end
function kernel(
        dpp::AbstractDPP{Vector{T},U},
        sym::Symbol,
        X::AbstractMatrix{T}
)::LA.HermOrSym{U,Matrix{U}} where {T,U}
    return _kernel_matrix(dpp, sym, X)
end
function kernel(
        dpp::AbstractDPP{T,U},
        sym::Symbol,
        X::Union{AbstractSet{T}, AbstractVector{T}},
        Y::Union{AbstractSet{T}, AbstractVector{T}}
)::Matrix{U} where {T,U}
    return _kernel_matrix(dpp, sym, X, Y)
end
function kernel(
        dpp::AbstractDPP{Vector{T},U},
        sym::Symbol,
        X::Union{AbstractSet{Vector{T}}, AbstractVector{Vector{T}}, AbstractMatrix{T}},
        Y::Union{AbstractSet{Vector{T}}, AbstractVector{Vector{T}}, AbstractMatrix{T}}
)::Matrix{U} where {T,U}
    return _kernel_matrix(dpp, sym, X, Y)
end
